![Build Status](https://gitlab.com/pages/nift/badges/master/build.svg)

---

Example Nift website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI](https://about.gitlab.com/gitlab-ci/), following the steps
defined in [`.gitlab-ci.yml`](https://gitlab.com/pages/nift/blob/master/.gitlab-ci.yml):

    pages:
      stage: deploy
      script:
      - mkdir .public
      - cp -r site/* .public
      - mv .public public
      artifacts:
        paths:
        - public
      only:
      - master

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://nift.cc/documentation/installing_nsm.html) Nift
1. Generate the website: `nsm build-updated`
1. Preview your project: pages are generated in the `site` directory
1. Push your project to gitlab.com

Read more at Nift's [tutorial page](https://nift.cc/documentation/nsm_tutorial.html) and [hosting your site page](https://www.nift.cc/documentation/hosting_your_site.html).

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages](https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages) and [project Pages](https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages).

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[<project>]: https://nift.cc/
[install]: https://nift.cc/documentation/installing_nsm.html
[tutorial]: https://nift.cc/documentation/get_started.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

----

